<?php

namespace Drupal\light_category\Plugin\Block;

use \Drupal\Core\Block\BlockBase;
use \Drupal\Core\Block\BlockPluginInterface;
use \Drupal\Core\Form\FormBuilderInterface;
use \Drupal\Core\Form\FormStateInterface;
use \Drupal\views\Views;
use \Drupal\taxonomy\Entity\Term;
use \Drupal\Core\Site\Settings;

/**
 * Provides a 'Light Category Block'.
 * 
 * @Block(
 * id = "light_category_block",
 * admin_label = @Translation("Light Category Block"),
 * category = @Translation("Views"),
 * )
 */
class LightCategoryBlock extends BlockBase implements BlockPluginInterface {

  /**
   * The light category trait
   */
  use \Drupal\light_category\Traits\LightCategoryTrait;
  use \Drupal\light_category\Traits\LightCategoryFormTrait;

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    $config =  $this->getDefaultConfiguration();
    $config['count']      = NULL;
    $config['arguments']  = !empty($this->configuration['arguments']) ? $this->configuration['arguments'] : NULL;
    $config['more-link']  = '';
    return $config;
}

  /**
   * {@inheritDoc}
   */
  public function getConfigurations(string $name) {
    $config = \Drupal::config('light_category.settings')->get();
    if( !empty($config['client-mode']) ) return !empty($config[$name]) ? $config[$name] :  $this->defaultConfiguration()[$name];
    if (!empty($this->configuration[$name])) return $this->configuration[$name];
    return !empty($config[$name]) ? $config[$name] : $this->defaultConfiguration()[$name];
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $form = array_merge($this->getLightCategoryForm(), $form);

    $form['system']['#attributes']['class'][] = 'hidden';
    $form['arguments']['#disabled']           = FALSE;
    $form['views']['boolean-filter']['#ajax'] = FALSE;
    $form['views']['view-mode']['#ajax']      = FALSE;
    $form['views']['display']['#ajax']        = FALSE;
    $form['views']['more-link'] = [
      '#type'         => 'textfield',
      '#title'        => t('Custom link to page'),
      '#default_value'=> $this->getConfigurations('term-alias') && !empty($this->getConfigurations('arguments')) ? Term::load($this->getConfigurations('arguments'))->url() : $this->getConfigurations('more-link'),
      '#states'           => [
        'invisible'       => [
          '#term-alias'   => ['checked' => TRUE]
        ],
      ],
    ];
    $form['count'] = [
      '#type'           => 'number',
      '#title'          => t('Count result for block'),
      '#default_value'  => $this->getConfiguration()['count'],
    ];

    if(!empty($form_state->getCompleteFormState()->getValue('settings'))){
      $value = $form_state->getCompleteFormState()->getValue('settings');
      $form['views']['display']['#options'] = !empty($value['views']['view']) ? $this->getViewPluginBlock(Views::getView($value['views']['view'])->storage->get('display')) : $this->getConfigurations('display');
    }

    if( $form_state->getCompleteFormState()->getTriggeringElement()['#id'] === 'vocabulary' ) {
      $vocabulary = $form_state->getCompleteFormState()->getValue('settings')['views']['vocabulary'];
      if(!empty($vocabulary) || $vocabulary === 'all')  $form['arguments']['#selection_settings'] = []; 
      $form['arguments']['#selection_settings']['target_bundles'] = [$form_state->getCompleteFormState()->getValue('settings')['views']['vocabulary']];
    }

    return $form;
  }

  /**
   * Ajax callback function
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return void
   */
  public function lightCategoryCallback(array &$form, FormStateInterface $form_state) {
    $triger =  $form_state->getTriggeringElement()['#id'];
    $el     = $form_state->getValue('settings');

    switch($triger) {

      case 'view':
        $form['settings']['views']['display']['#options']        = $display = $this->getViewPluginBlock(Views::getView($el['views']['view'])->storage->get('display'));
        $form['settings']['views']['view-mode']['#options']      = array_merge(['none'=>'- None -'], $this->getSelect('viewMode', Views::getView($el['views']['view'])));
        $form['settings']['views']['boolean-filter']['#options'] = array_merge(['none'=>'- None -'], $this->getSelect('fieldBool', Views::getView($el['views']['view'])));
      break;

      case 'vocabulary':
        $form['settings']['arguments']['#value'] = '';
      break;

    }
    $form_state->setRebuild(TRUE);
    return $form['settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    $form_value                             = $form_state->getValue('views');
    $this->configuration['view']            = $form_state->getValue('views')['view'];
    $this->configuration['display']         = $form_state->getValue('views')['display'];
    $this->configuration['view-mode']       = $form_state->getValue('views')['view-mode'];
    $this->configuration['vocabulary']      = $form_state->getValue('views')['vocabulary'];
    $this->configuration['boolean-filter']  = $form_state->getValue('views')['boolean-filter'];
    $this->configuration['term-alias']      = $form_state->getValue('views')['term-alias'];
    $this->configuration['more-link']       = $form_state->getValue('views')['more-link'];
    $this->configuration['count']           = $form_state->getValue('count');
    $this->configuration['arguments']       = $form_state->getValue('arguments');
    parent::blockSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function buildExclusion(string $action) {
    $build;
    switch ($action) {

      case 'hide':
        $build = [];
      break;

      case 'error':
        $build = [
          '#prefix' => '<div class="view-empty">',
          '#markup' => $this->t('Sorry, but it looks like you have deleted / empty the view / display of the view, or the arguments are incorrect.'),
          '#suffix' => '</div>',
        ];
      break;

    }
    return $build;
  }


  /**
   * {@inheritdoc}
   */
  public function build() {

    $view           = $this->configuration['view'];
    $display        = $this->configuration['display'];
    $arguments      = $this->configuration['arguments'];
    $booleanFilter  = $this->configuration['boolean-filter'];
    $viewMode       = $this->configuration['view-mode'];
    $termAlias      = $this->configuration['term-alias'];
    $moreLink       = $this->configuration['more-link'];
    $count          = $this->configuration['count'];

    /**
     * Init view
     */
    $view = Views::getView($view);
    if(empty($view)) return $this->buildExclusion('error');
    if(!$view->access($display)) return $this->buildExclusion('error');

    $view->setDisplay($display);
    $view = $view->getDisplay();
    $view->view->setArguments([$arguments]);

    /**
     * Set row style
     */
    if(!empty($viewMode) && $viewMode != 'none') {
      $row_option = $view->view->display_handler->getOption('row');
      if(!empty($row_option['options']['view_mode'])) {
        $row_option['options']['view_mode'] = $viewMode;
        $view->view->display_handler->overrideOption('row',$row_option );
      }
    }

    /**
     * Set filter
     */
    if(!empty($booleanFilter) && $booleanFilter != 'none') {

      $booleanFilter = \Drupal::entityTypeManager()->getStorage('field_config')->load($this->configuration['boolean-filter'])->toArray();
      $view->view->addHandler($display, 'filter',  $booleanFilter['entity_type'] . '__' . $booleanFilter['field_name'], $booleanFilter['field_name'] . '_value', [
        'operator' => "=",
        'value' =>  "1",
        'plugin_id' =>  "boolean",
      ]);
    } 

    /**
     * Set more link
     */
    if($view->view->display_handler->getOption('use_more')){
      $view->view->display_handler->overrideOption('link_display', 'custom_url');
      if($termAlias && !empty($arguments)){
        if(!empty(Term::load($arguments))){
          $view->view->display_handler->overrideOption('link_url', Term::load($arguments)->url());
        }
      } elseif(!empty($moreLink)) {
        $view->view->display_handler->overrideOption('link_url', $moreLink);
      }
    }

    /**
     * Set count result for block
     */
    if(!empty($count)){
      $pager = $view->view->display_handler->getOption('pager');
      if(!empty($pager['options']['items_per_page'])){
        $pager['options']['items_per_page'] = $count;
        $pager = $view->view->display_handler->setOption('pager', $pager);
      }
    }

    $view->preExecute();
    $view->execute();
    $view =  $view->buildRenderable();

    if($view['#view']->getDisplay()->options['block_hide_empty'] && empty($view['#view']->result)) return $this->buildExclusion('hide');
    
    $view['#view_id'] = $view['#view']->storage->id();
    $view['#view_display_show_admin_links'] = $view['#view']->getShowAdminLinks();
    $view['#view_display_plugin_id'] = $view['#view']->display_handler->getPluginId();

    views_add_contextual_links($view, 'block', $display);
    return  $view;
  }
}