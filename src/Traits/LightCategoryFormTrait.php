<?php

namespace Drupal\light_category\Traits;

use \Drupal\Core\Form\ConfigFormBase;
use \Drupal\Core\Form\FormStateInterface;
use \Drupal\views\Views;
use \Drupal\light_category\Form\LightCategorySettingsForm;
use \Drupal\taxonomy\Entity\Term;

/**
 * Provides a 'Light Category trait'.
 */
trait LightCategoryFormTrait {

  /**
   * {@inheritDoc}
   * return default configuration
   */
  public function getDefaultConfiguration() {
    return array(
      'hide-message'       => 0,
      'client-mode'        => 0,
      'view'               => key($this->getSelect('onlyDisplayView')),
      'display'            => NULL,
      'view-mode'          => NULL,
      'vocabulary'         => key($this->getSelect('vocabulary')),
      'term-alias'         => 0,
      'boolean-filter'     => NULL,
      'arguments'          => NULL
    );
  }

  /**
   * Return form structure
   *
   * @return array
   */
  public function getLightCategoryForm() {
      
    /**
     * Group for elements
     */
    $form            = [];
    $form['#id']     = 'ajax-wrapper-callback';
    $form['#prefix'] = '<div id="ajax-wrapper">';
    $form['#suffix'] = '</div>';

    $form['system'] = [
      '#type'   => 'details',
      '#title'  => $this->t('System settings'),
      '#open'   => 'open',
    ];

    $form['views'] = [
      '#type'     => 'details',
      '#title'    => $this->t('View settings block'),
      '#open'     => 'open',
      '#states'   => [
        'invisible' => [
          '#edit-client-mode' => ['checked' => TRUE]
        ],
      ],
    ];

    /**
     * Form element
     */
    $form['message'] = [
      '#type'             => 'webform_message',
      '#message_type'     => 'warning',
      '#message_message'  => $this->t('This module allows to use only views that have a plug-in block'),
      '#weight'           => -1000,
      '#states'           => [
        'invisible'         => [
          '#edit-hide-message' => ['checked' => TRUE]
        ],
      ],
    ];

    $form['system']['hide-message'] = [
      '#type'           => 'checkbox',
      '#title'          => $this->t('I understood the settings of this module. ( Hide message )'),
      '#default_value'  => $this->getConfigurations('hide-message'),
      '#id'             => 'edit-hide-message'
    ];

    $form['system']['client-mode'] = [
      '#type'           => 'checkbox',
      '#title'          => $this->t('Client mode'),
      '#default_value'  => $this->getConfigurations('client-mode'),
      '#id'             => 'edit-client-mode'
    ];

    $form['views']['view'] = [
      '#type'           => 'select',
      '#title'          => $this->t('Select a views'),
      '#options'        => $this->getSelect('onlyDisplayView'),
      '#default_value'  => $this->getConfigurations('view'),
      '#id'             => 'view',
      '#required'        => TRUE,
      '#ajax'           => [
        'event'           => 'change',
        'method'          => 'replace',
        'callback'        => [$this, 'lightCategoryCallback'],
        'wrapper'         => 'ajax-wrapper'
      ],
    ];
    
    $viewOptions = !empty(Views::getView($this->getConfigurations('view'))) ? $this->getConfigurations('view') : key($this->getSelect('onlyDisplayView'));
    $form['views']['display'] = [
      '#type'           => 'select',
      '#title'          => $this->t('Select a display'),
      '#options'        => $this->getViewPluginBlock(Views::getView($viewOptions)->storage->get('display')) ,
      '#default_value'  => $this->getConfigurations('display'),
      '#validated'      => TRUE,
      '#required'        => TRUE,
      '#id'             => 'display',
      '#ajax'           => [
        'event'           => 'change',
        'callback'        => [$this, 'lightCategoryCallback'],
        'method'          => 'replace',
      ],
    ];

    $form['views']['view-mode'] = [
      '#type'           => 'select',
      '#title'          => $this->t('Select a style view'),
      '#description'    => $this->t('This apply if your view use display — content'),
      '#options'        => array_merge(['none'=>'- None -'], $this->getSelect('viewMode', Views::getView($viewOptions))),
      '#default_value'  => $this->getConfigurations('view-mode'),
      '#validated'      => TRUE,
      '#id'             => 'view-mode',
      '#ajax'           => [
        'event'           => 'change',
        'callback'        => [ $this, 'lightCategoryCallback' ],
      ],
    ];

    $form['views']['vocabulary'] = [
      '#type'           => 'select',
      '#title'          => $this->t('Only vocabulary'),
      '#options'        => array_merge(['all'=> 'All'], $this->getSelect('vocabulary')),
      '#default_value'  => $this->getConfigurations('vocabulary'),
      '#id'             => 'vocabulary',
      '#ajax'           => [
        'event'           => 'change',
        'method'          => 'replace',
        'callback'        => [$this, 'lightCategoryCallback'],
        'wrapper'         => 'ajax-wrapper'
      ],
    ];

    $form['views']['term-alias'] = [
      '#type'           => 'checkbox',
      '#title'          => $this->t('Use term alias'),
      '#description'    => $this->t('Use term alias as "Details" link'),
      '#default_value'  => $this->getConfigurations('term-alias'),
      '#id'             => 'term-alias'
    ];

    $form['views']['boolean-filter'] = [
      '#type'           => 'select',
      '#title'          => $this->t('Boolean filter'),
      '#options'        =>  array_merge(['none'=>'- None -'], $this->getSelect('fieldBool', Views::getView($viewOptions))),
      '#default_value'  => $this->getConfigurations('boolean-filter'),
      '#validated'      => TRUE,
      '#id'             => 'boolean-filter',
      '#ajax'           => [
        'event'           => 'change',
        'callback'        => [$this, 'lightCategoryCallback'],
      ],
    ];

    $form['arguments'] = [
      '#type'               => 'entity_autocomplete',
      '#title'              => $this->t('Taxonomy term'),
      '#target_type'        => 'taxonomy_term',
      '#disabled'           => 'disable',
      '#default_value'      => !empty($this->getConfigurations('arguments')) ? Term::load($this->getConfigurations('arguments')) : '',
      '#autocreate'         => FALSE,
      '#tag'                => FALSE,
      '#id'                 => 'arguments',
      '#selection_settings' => $this->getConfigurations('vocabulary') === 'all' ? [] :[
        'target_bundles'      => [$this->getConfigurations('vocabulary')]
      ],
    ];
    return $form;
  }
}