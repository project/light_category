<?php

namespace Drupal\light_category\Traits;

use \Drupal\light_category\Form\LightCategorySettingsForm;

/**
 * Provides a 'Light Category trait'.
 */
trait LightCategoryTrait {

   /**
    * Return options array for select element
    *
    * @param string $str : 
    * onlyDisplayView
    * view
    * display
    * vocabulary
    *
    *
    * @return void
    */
   public static function getSelect( string $str, $objHelp = NULL )
   {
      $manager = \Drupal::entityTypeManager();
      $manager_view = $manager->getStorage('view');
      

      switch( $str ) {

         case 'onlyDisplayView':
            $viewSelect = [];
            foreach (  $manager_view->loadMultiple() as $key=>$view ) {
               foreach ( $view->get('display') as $itemDisplay ) {
                  if ( $itemDisplay['display_plugin'] != 'block' ) continue;
                  $viewSelect[$key] = $key;
               }
            }
            return $viewSelect;
         break;

         case 'view':
            return self::forEach( $manager_view->loadMultiple() );
         break;
         case 'vocabulary':
            return self::forEach( $manager->getStorage('taxonomy_vocabulary')->loadMultiple() );
         break;

         case 'viewMode':
            $viewMode = [];
            $type = !empty(  $objHelp->getHandlers('filter')['type'] ) ?  $objHelp->getHandlers('filter')['type']['entity_type'] : TRUE;
            foreach (  $manager->getStorage('entity_view_display')->loadMultiple() as $key=>$mode ) {
               if( $mode->getTargetEntityTypeId()  != $type ) continue;
               $viewMode[$mode->getMode()] = $mode->id();
            }
            return $viewMode;
         break;

         case 'fieldBool':
            $fieldBool = [];
            $type = !empty(  $objHelp->getHandlers('filter')['type'] ) ?  $objHelp->getHandlers('filter')['type']['entity_type'] : FALSE;
            if ( $type ) {
               if( !empty( $objHelp->getHandlers('filter')['type']['value'] ) ){
                  $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions( $type , key($objHelp->getHandlers('filter')['type']['value'])  );
                  foreach( $fields as $key=>$field ) {
                     if( $field->getType() != 'boolean'  ) continue; 
                     if( !method_exists( $field, 'get' ) ) continue;
                     if( $field->get('entity_type') === $objHelp->getHandlers('filter')['type']['entity_type'] ) $fieldBool[$field->id()] = $field->getLabel();
                  }
               }
            }
            return !empty( $fieldBool ) ? $fieldBool : ['none'=>'- None -'];
         break;

         default:
            return [];
         break;
      }
   }
   
   /**
    * Return options array for select element
    *
    * @param array $arr
    */
   public static function forEach( array $arr )
   {
      $selectList = [];
      foreach ( $arr as $key=>$value ) {
         $selectList[$key] =   $key;
      }
      return $selectList;
   }

   /**
    * Return options array for select element ( display view - block )
    *
    * @param array $arr
    */
    public static function getViewPluginBlock( array $arr )
    {
      $selectList = [];
      foreach ( $arr as $key=>$value ) {
         if ( $value['display_plugin'] != 'block' ) continue;
         $selectList[$key] =   $key;
      }
      return $selectList;
    }

}