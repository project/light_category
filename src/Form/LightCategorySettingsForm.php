<?php

namespace Drupal\light_category\Form;

use \Drupal\Core\Form\ConfigFormBase;
use \Drupal\Core\Form\FormStateInterface;
use \Drupal\views\Views;

class LightCategorySettingsForm extends ConfigFormBase {

  /**
   * The light category trait
   */
  use \Drupal\light_category\Traits\LightCategoryTrait;
  use \Drupal\light_category\Traits\LightCategoryFormTrait;

  /**
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'light_category_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['light_category.settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return $this->getDefaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function getConfigurations(string $name) {
    $config = $this->config('light_category.settings')->get();
    return !empty($config[$name]) ? $config[$name] : $this->defaultConfiguration()[$name];
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    return parent::buildForm($form = $this->getLightCategoryForm() , $form_state);
  }

  /**
   * Ajax callback for form
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return void
   */
  public function lightCategoryCallback(array &$form, FormStateInterface $form_state) {
    $triger = $form_state->getTriggeringElement()['#name'];
    switch($triger) {

      case 'view':
        $form['views']['display']['#options'] = $display = $this->getViewPluginBlock(Views::getView($form_state->getValue('view'))->storage->get('display'));
        if(count($display) <= 1) {
          $this->config('light_category.settings')->set('display', $form_state->getValue('display'))->save();
        }
        $form['views']['view-mode']['#options']       = array_merge(['none'=>'- Not use -'], $this->getSelect('viewMode', Views::getView($form_state->getValue('view'))));
        $form['views']['boolean-filter']['#options']  = array_merge(['none'=>'- None -'], $this->getSelect('fieldBool', Views::getView($form_state->getValue('view'))));

        $this->config('light_category.settings')->set('view', $form_state->getValue('view'))->save();
        $this->config('light_category.settings')->set('view-mode', $form_state->getValue('view-mode'))->save();
      break;

      case 'display':
        $this->config('light_category.settings')->set('view', $form_state->getValue('view'))->save();
        $this->config('light_category.settings')->set('display', $form_state->getValue('display'))->save();
      break;

      case 'view-mode':
        $this->config('light_category.settings')->set('view-mode', $form_state->getValue('view-mode'))->save();
      break;

      case 'vocabulary':
        $this->config('light_category.settings')->set('vocabulary', $form_state->getValue('vocabulary'))->save();
      break;

      case 'boolean-filter':
        $this->config('light_category.settings')->set('boolean-filter', $form_state->getValue('boolean-filter'))->save();
      break;

    }
    $form_state->setRebuild(TRUE);
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('light_category.settings')->set('hide-message', $form_state->getValue('hide-message'))->save();
    $this->config('light_category.settings')->set('client-mode', $form_state->getValue('client-mode'))->save();

    $this->config('light_category.settings')->set('view', $form_state->getValue('view'))->save();
    $this->config('light_category.settings')->set('display', $form_state->getValue('display'))->save();
    $this->config('light_category.settings')->set('view-mode', $form_state->getValue('view-mode'))->save();
    $this->config('light_category.settings')->set('vocabulary', $form_state->getValue('vocabulary'))->save();
    $this->config('light_category.settings')->set('term-alias', $form_state->getValue('term-alias'))->save();
    $this->config('light_category.settings')->set('boolean-filter', $form_state->getValue('boolean-filter'))->save();
    parent::submitForm($form, $form_state);
  }
}