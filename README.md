CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Goal
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module is intended to quickly change the category of material in terms of
taxonomy.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/light_category

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/light_category

 * How to settings view for Light Category Block:
   https://www.youtube.com/watch?v=nd0nlvDIHYw

 * Youtube — How to use module Light Category Block:
   https://www.youtube.com/watch?v=SdQXeFnlwo4


GOAL
----

 * Convenient change of the product output by category for the client
 * Reducing the number of displays of the type
 * Reuse one a block view


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Light Category Block module as you would normally install
   a contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Structure > Block layout.
    3. Place block and in the list of available blocks choose "Light Category Block".
    4. In "Select a views", "Select a display" and "Select a style view" choose
       the appropriate term in the autocomplete. Save block.


MAINTAINERS
-----------

 * Artur Tkachenko (UsingSession) - https://www.drupal.org/u/usingsession
